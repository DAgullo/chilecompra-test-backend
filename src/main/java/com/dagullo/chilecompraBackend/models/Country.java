package com.dagullo.chilecompraBackend.models;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import com.fasterxml.jackson.annotation.JsonProperty;

@JsonIgnoreProperties(ignoreUnknown = true)
public class Country {

    @JsonProperty("Country")
    private String country;
    @JsonProperty("Slug")
    private String slug;
    @JsonProperty("TotalConfirmed")
    private Integer totalConfirmed;
    @JsonProperty("TotalDeaths")
    private Integer totalDeaths;
    @JsonProperty("TotalRecovered")
    private Integer totalRecovered;

    public Country () {
    }

    public String getCountry () {
        return this.country;
    }

    public String getSlug () {
        return this.slug;
    }

    public Integer getTotalConfirmed () {
        return this.totalConfirmed;
    }

    public Integer getTotalDeaths () {
        return this.totalDeaths;
    }

    public Integer getTotalRecovered () {
        return this.totalRecovered;
    }

    public void setCountry(String country) {
        this.country = country;
    }

    public void setSlug(String slug) {
        this.slug = slug;
    }

    public void setTotalConfirmed(Integer totalConfirmed) {
        this.totalConfirmed = totalConfirmed;
    }

    public void setTotalDeaths(Integer totalDeaths) {
        this.totalDeaths = totalDeaths;
    }

    public void setTotalRecovered(Integer totalRecovered) {
        this.totalRecovered = totalRecovered;
    }

    @Override
    public String toString() {
        return "{" +
                "Country:'" + country + '\'' +
                ", Slug:'" + slug + '\'' +
                ", TotalConfirmed:" + totalConfirmed +
                ", TotalDeaths:" + totalDeaths +
                ", TotalRecovered:" + totalRecovered +
                '}';
    }
}
