package com.dagullo.chilecompraBackend.models;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import com.fasterxml.jackson.annotation.JsonProperty;

import java.time.ZonedDateTime;
import java.util.Comparator;

@JsonIgnoreProperties(ignoreUnknown = true)
public class CountryLocation {

    @JsonProperty("Confirmed")
    private Integer confirmed;
    @JsonProperty("Deaths")
    private Integer deaths;
    @JsonProperty("Recovered")
    private Integer recovered;
    @JsonProperty("Active")
    private Integer active;
    @JsonProperty("Date")
    private ZonedDateTime date;

    public CountryLocation () {
    }

    public CountryLocation (ZonedDateTime date, Integer confirmed, Integer deaths, Integer recovered, Integer active) {
        this.date = date;
        this.confirmed = confirmed;
        this.deaths = deaths;
        this.recovered = recovered;
        this.active = active;
    }

    public Integer getConfirmed() {
        return confirmed;
    }

    public void setConfirmed(Integer confirmed) {
        this.confirmed = confirmed;
    }

    public Integer getDeaths() {
        return deaths;
    }

    public void setDeaths(Integer deaths) {
        this.deaths = deaths;
    }

    public Integer getRecovered() {
        return recovered;
    }

    public void setRecovered(Integer recovered) {
        this.recovered = recovered;
    }

    public Integer getActive() {
        return active;
    }

    public void setActive(Integer active) {
        this.active = active;
    }

    public ZonedDateTime getDate() {
        return date;
    }

    public void setDate(ZonedDateTime date) {
        this.date = date;
    }

    @Override
    public String toString() {
        return "CountryLocation{" +
                "confirmed=" + confirmed +
                ", deaths=" + deaths +
                ", recovered=" + recovered +
                ", active=" + active +
                ", date=" + date +
                '}';
    }

    public static class DateComparator implements Comparator<CountryLocation> {

        @Override
        public int compare(CountryLocation o1, CountryLocation o2) {
            return o1.getDate().compareTo(o2.getDate());
        }
    }
}
