package com.dagullo.chilecompraBackend.models;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import com.fasterxml.jackson.annotation.JsonProperty;

import java.util.List;

@JsonIgnoreProperties(ignoreUnknown = true)
public class CovidSummary {

    @JsonProperty("Countries")
    private List<Country> countries;

    public List<Country> getCountries() {
        return this.countries;
    }

    public void setCountries(List<Country> countries) {
        this.countries = countries;
    }

    @Override
    public String toString() {
        return "{" +
                "countries:" + countries.toString() +
                '}';
    }
}
