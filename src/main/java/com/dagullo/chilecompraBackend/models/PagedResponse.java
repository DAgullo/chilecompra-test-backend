package com.dagullo.chilecompraBackend.models;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import com.fasterxml.jackson.annotation.JsonProperty;

import java.util.List;

@JsonIgnoreProperties(ignoreUnknown = true)
public class PagedResponse<T> {
    @JsonProperty("totalPages")
    private final Integer totalPages;
    @JsonProperty("count")
    private final Integer count;
    @JsonProperty("page")
    private final Integer page;
    @JsonProperty("size")
    private final Integer size;
    @JsonProperty("response")
    private final List<T> response;


    public PagedResponse(Integer totalPages, Integer count, Integer page, Integer size, List<T> response) {
        this.totalPages = totalPages;
        this.count = count;
        this.page = page;
        this.size = size;
        this.response = response;
    }

    public Integer getTotalPages() {
        return totalPages;
    }

    public Integer getPage() {
        return page;
    }

    public List<T> getResponse() {
        return response;
    }

    public Integer getCount() {
        return count;
    }

    public Integer getSize() {
        return size;
    }
}
