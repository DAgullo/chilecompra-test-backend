package com.dagullo.chilecompraBackend.controllers;

import com.dagullo.chilecompraBackend.exceptions.PageNotFoundException;
import com.dagullo.chilecompraBackend.models.CountryLocation;
import com.dagullo.chilecompraBackend.models.CovidSummary;
import com.dagullo.chilecompraBackend.models.PagedResponse;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.client.RestTemplate;

import java.time.ZonedDateTime;
import java.util.*;
import java.util.function.Supplier;
import java.util.stream.Collectors;
import java.util.stream.Stream;

@CrossOrigin
@RestController
public class CovidController {

    RestTemplate restTemplate;

    public CovidController () {
        this.restTemplate = new RestTemplate();
    }

    @GetMapping("/statistics")
    public CovidSummary getWorldStats () {
        CovidSummary summary = restTemplate.getForObject("https://api.covid19api.com/summary", CovidSummary.class);
        return summary;
    }

    @GetMapping(value = "/statistics/{slug}", params = { "page" , "size"})
    public PagedResponse<CountryLocation> getCountryStats (@PathVariable String slug, @RequestParam Integer page, @RequestParam Integer size) {
        ResponseEntity<CountryLocation[]> summary = restTemplate.getForEntity("https://api.covid19api.com/country/" + slug, CountryLocation[].class);
        final List<CountryLocation> locations = Arrays.asList(summary.getBody());
        List<CountryLocation> summarizedLocations = new ArrayList<>();
        final Map<ZonedDateTime, List<CountryLocation>> map = locations.stream().collect(Collectors.groupingBy(row -> row.getDate()));
        for (final Map.Entry<ZonedDateTime, List<CountryLocation>> entry : map.entrySet()) {
            final Supplier<Stream<CountryLocation>> collection = () -> entry.getValue().stream();
            summarizedLocations.add(new CountryLocation(
                    entry.getKey(),
                    collection.get().mapToInt(row -> row.getConfirmed()).sum(),
                    collection.get().mapToInt(row -> row.getDeaths()).sum(),
                    collection.get().mapToInt(row -> row.getRecovered()).sum(),
                    collection.get().mapToInt(row -> row.getActive()).sum()
            ));
        }
        Collections.sort(summarizedLocations, new CountryLocation.DateComparator());
        final Integer pageTotal = new Double(Math.ceil((((float) summarizedLocations.size()) / size))).intValue();
        if (pageTotal <= page) {
            throw new PageNotFoundException(page);
        }
        return new PagedResponse<>(
                pageTotal,
                summarizedLocations.size(),
                page,
                size,
                summarizedLocations.subList((size * (page)), (size * (page)) + size)
        );
    }

    @GetMapping(value = "/statistics/{slug}", params = { "page"})
    public PagedResponse<CountryLocation> getCountryStats (@PathVariable String slug, @RequestParam Integer page) {
        return getCountryStats(slug, page, 25);
    }

    @GetMapping(value = "/statistics/{slug}")
    public PagedResponse<CountryLocation> getCountryStats (@PathVariable String slug) {
        return getCountryStats(slug, 0, 25);
    }

}
