package com.dagullo.chilecompraBackend.controllers;

import org.springframework.web.bind.annotation.RestController;
import org.springframework.web.bind.annotation.RequestMapping;

@RestController
public class BasicController {

    @RequestMapping("/")
    public String index () {
        return "Nothing to see here!";
    }
}
