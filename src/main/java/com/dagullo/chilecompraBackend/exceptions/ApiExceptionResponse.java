package com.dagullo.chilecompraBackend.exceptions;

import org.springframework.http.HttpStatus;

import java.time.ZonedDateTime;

public class ApiExceptionResponse {

    private final String message;
    private final HttpStatus statusCode;
    private final ZonedDateTime timestamp;

    public ApiExceptionResponse(String message, HttpStatus statusCode, ZonedDateTime timestamp) {
        this.message = message;
        this.statusCode = statusCode;
        this.timestamp = timestamp;
    }


    public String getMessage() {
        return message;
    }

    public HttpStatus getStatusCode() {
        return statusCode;
    }

    public ZonedDateTime getTimestamp() {
        return timestamp;
    }
}
