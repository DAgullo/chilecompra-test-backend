package com.dagullo.chilecompraBackend.exceptions;

public class PageNotFoundException extends RuntimeException {

    public PageNotFoundException (Integer page) {
        super("Page does not exist: "+ page);
    }

    public PageNotFoundException (Integer page, Throwable cause) {
        super("Page does not exist: "+ page, cause);
    }
}
