package com.dagullo.chilecompraBackend.exceptions;

import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.ControllerAdvice;
import org.springframework.web.bind.annotation.ExceptionHandler;

import java.time.ZoneId;
import java.time.ZonedDateTime;

@ControllerAdvice
public class ApiExceptionHandler {

    @ExceptionHandler(value = { PageNotFoundException.class })
    public ResponseEntity<Object> handlePageNotFoundException(PageNotFoundException e) {
        HttpStatus code =  HttpStatus.BAD_REQUEST;

        ApiExceptionResponse response = new ApiExceptionResponse(e.getMessage(), code, ZonedDateTime.now(ZoneId.of("Z")));

        return new ResponseEntity<>(response, code);
    }
}
