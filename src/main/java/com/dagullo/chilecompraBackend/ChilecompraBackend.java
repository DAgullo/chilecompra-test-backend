package com.dagullo.chilecompraBackend;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class ChilecompraBackend {

	public static void main (String[] args) {
        SpringApplication.run(ChilecompraBackend.class, args);
	}


}
