# Prueba Técnica Chilecompra - Backend

## About

Proyecto simple generado a través de Spring Boot 2.4.0 para el Backend de la Prueba Técnica para Chilecompra.

## Instalación

El proyecyo requiere de JDK 1.8 para ser ejecutado. Una vez instalada la versión de Java, solo basta con extraer el proyecto.

## Ejecución

Entrar al directorio raíz del proyecto, y ejecutar **./gradlew bootRun**. A su vez, se puede generar un .jar usando el comando **./gradlew build**, y posteriormente corriendo el servlet de Tomcat generado en build/libs/ con java -jar ./build/libs/chilecompraBackend-0.1.0.jar

## Endpoints

El sistema consiste de dos endpoints: 

  - **/statistics** que entrega la información requerida por la vista de tarjetas de país
  - **/statistics/:slug** que entrega la información requerida por la tabla de estadísticas diarias de un país, a la cual se le pueden pasar los query params *page* y *size*.
  



